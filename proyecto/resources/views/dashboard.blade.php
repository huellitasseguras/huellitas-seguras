<x-app-layout>
    <div>
        <section class="relative pt-12 bg-blueGray-50">
            <div class="items-center flex flex-wrap">
                <div class="w-full md:w-4/12 ml-auto mr-auto px-4">
                    <img alt="..." class="max-w-full rounded-lg shadow-lg"
                        src="https://images.unsplash.com/photo-1555212697-194d092e3b8f?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=634&amp;q=80">
                </div>
                <div class="w-full md:w-5/12 ml-auto mr-auto px-4">
                    <div class="md:pr-12">

                        <h3 class="text-3xl font-semibold">Resumen de Huellitas Seguras</h3>
                        <p class="mt-4 text-lg leading-relaxed text-blueGray-500">
                            La idea de este proyecto consiste en fabricar y vender placas para mascotas impresas por
                            medio de las impresoras 3D a la vez que los datos de cada placa será almacenado en la página
                            web de la empresa para más información de la mascotas como el historial médico (por si
                            padece de alguna complicación médica o alguna otra información importante). Las placas para
                            mascotas serán de varios diseños que contarán con información de suma importancia para
                            identificar a las mascotas en caso de extravío, el sector en el que se podrá enfocar los
                            accesorios, en este caso sería accesorios para mascotas.

                            .
                        </p>
                        <ul class="list-none mt-6">
                            <li class="py-2">
                                <div class="flex items-center">
                                    <div>
                                        <span
                                            class="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-pink-600 bg-pink-200 mr-3"><i
                                                class="fas fa-fingerprint"></i></span>
                                    </div>
                                    <div>
                                        <h4 class="text-blueGray-500">
                                            El mercado que manejamos será accesorios para mascotas
                                        </h4>
                                    </div>
                                </div>
                            </li>
                            <li class="py-2">
                                <div class="flex items-center">
                                    <div>
                                        <span
                                            class="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-pink-600 bg-pink-200 mr-3"><i
                                                class="fab fa-html5"></i></span>
                                    </div>
                                    <div>
                                        <h4 class="text-blueGray-500">La empresa se ubicará en Maxcanú</h4>
                                    </div>
                                </div>
                            </li>
                            <li class="py-2">
                                <div class="flex items-center">
                                    <div>
                                        <span
                                            class="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-pink-600 bg-pink-200 mr-3"><i
                                                class="far fa-paper-plane"></i></span>
                                    </div>
                                    <div>
                                        <h4 class="text-blueGray-500">Emprendimiento.</h4>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <footer class="relative  pt-8 pb-6 mt-8">
                <div class="container mx-auto px-4">
                    <div class="flex flex-wrap items-center md:justify-between justify-center">
                        <div class="w-full md:w-6/12 px-4 mx-auto text-center">

                        </div>
                    </div>
                </div>
            </footer>
        </section>
    </div>


</x-app-layout>
